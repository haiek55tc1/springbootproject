package com.devpro.jv23TranDinhHai.services.impl;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.devpro.jv23TranDinhHai.dto.UserSearch;
import com.devpro.jv23TranDinhHai.model.BaseEntity;
import com.devpro.jv23TranDinhHai.model.HoSoEntity;
import com.devpro.jv23TranDinhHai.model.User;
import com.devpro.jv23TranDinhHai.services.BaseService;
import com.devpro.jv23TranDinhHai.services.PagerData;



@Service
public class UserService extends BaseService<User>{
	
	
	@Autowired
	private HoSoService hoSoService;
	@Override
	protected Class<User> clazz() {
		return User.class;
	}
	
	
	//kiem tra xem adim co upload anh hay khong
	
		private boolean isEmptyUploadFile(MultipartFile[] images) {
			if(images == null || images.length <= 0) 
				return true;	
			
			
			if(images.length == 1 && images[0].getOriginalFilename().isEmpty()) return true;
			
			return false;
		}
		//kiem tra xem adim co upload anh hay khong
		private boolean isEmptyUploadFile(MultipartFile image) {
			return image == null || image.getOriginalFilename().isEmpty();
		}
		
		
		//Tao ten file upload
		private String getUniqueFileName(String fileName) {
			String[] splitFilName = fileName.split("\\.");
			return splitFilName[0] + System.currentTimeMillis() + "." + splitFilName[1];
		}
		@Transactional
		public User addUser(User user, MultipartFile matTruoc, MultipartFile matSau)
				throws IllegalStateException, IOException {
			HoSoEntity hoSo = new HoSoEntity();
			//Kiểm tra xem admin có đẩy avt lên không
			if(!isEmptyUploadFile(matTruoc)) {//Nếu có đẩy lên thì
				
				//Lấy tên file
				String fileName = getUniqueFileName(matTruoc.getOriginalFilename());
				
				//Tạo đường dẫn tới file chứa avt
				String pathMatTruoc = "/Users/apple/Desktop/upload/product/HoSo/MatTruoc/" + fileName;
				
				//Lưu avt vào đường dẫn vừa tạo 
				
				matTruoc.transferTo(new File(pathMatTruoc));
				
				
				hoSo.setMatTruoc("/product/HoSo/MatTruoc/"+ fileName);
				
				
			}
			
			
			//Kiểm tra xem admin có đẩy avt lên không
			if(!isEmptyUploadFile(matSau)) {//Nếu có đẩy lên thì
				
				//Lấy tên file
				String fileName = getUniqueFileName(matSau.getOriginalFilename());
				
				//Tạo đường dẫn tới file chứa avt
				String pathMatSau = "/Users/apple/Desktop/upload/product/HoSo/MatSau/" + fileName;
				
				//Lưu avt vào đường dẫn vừa tạo 
				matSau.transferTo(new File(pathMatSau));
		
				hoSo.setMatSau("/product/HoSo/MatSau/"+ fileName);
				
			}
			
			user.addImageHoSo(hoSo);
			
			return super.saveOrUpdate(user);
			
		}
		
		@Transactional
		public User editUser(User user, MultipartFile matTruoc, MultipartFile matSau) throws IllegalStateException, IOException {
			
			//lấy thông tin cũ ra
			List<HoSoEntity> hoSoByIdList =  hoSoService.getEntitiesByNativeSQL("select * from ho_so where id_user = "+ user.getId()+ ";");
			HoSoEntity hoSoById = null;
			
			for(int i = 0; i < hoSoByIdList.size(); i++) {
				if(hoSoByIdList.get(0).getUsers().getId() == user.getId()) {
					hoSoById = hoSoByIdList.get(i);
				}
				
			}
			//kiểm tra xem có đẩy ảnh lên không
			if(!isEmptyUploadFile(matTruoc)) {//Có đẩy lên
				//Xoá ảnh cũ
				new File("/Users/apple/Desktop/upload/" + hoSoById.getMatTruoc()).delete();
				
				//updatte ảnh mới
				String fileName = getUniqueFileName(matTruoc.getOriginalFilename());
				matTruoc.transferTo(new File("/Users/apple/Desktop/upload/product/HoSo/MatTruoc/" + fileName));
				hoSoById.setMatTruoc("/product/HoSo/MatTruoc/"+fileName);
			}else {
				//Nếu không sửa dùng lại ảnh cũ
			hoSoById.setMatTruoc(hoSoById.getMatTruoc());
			}
			
			if(!isEmptyUploadFile(matSau)) {
				//Xoa anh cu
				
				new File("/User/apple/Desktop/upload/" + hoSoById.getMatSau()).delete();
				
				//
				String fileName = getUniqueFileName(matSau.getOriginalFilename());
				matSau.transferTo(new File("/Users/apple/Desktop/upload/product/HoSo/MatSau/" + fileName));
				hoSoById.setMatSau( "/product/HoSo/MatSau/"+fileName);
			}else {
				hoSoById.setMatSau(hoSoById.getMatSau());
			}
			
			return  super.saveOrUpdate(user);
			
		}
		
		
		public PagerData<User> userSearch(UserSearch userModel){
			
			String sql = "select * from user_customer  where 1 = 1 ";
			if(userModel != null) {
				
				if(userModel.getStatusId() != null && !"0".equals(userModel.getStatusId())) {
					
					sql += "and (status_id = "+ Integer.parseInt(userModel.getStatusId()) + ")";
					
				
					
				}
				
				if(!StringUtils.isEmpty(userModel.getKeyword())) {
					
					sql += "and (ten like '%"+ userModel.getKeyword() + "%' or "+ 
					"phone_number = '"+ userModel.getKeyword()+ "')";
					
					
//					System.out.println(sql);
					
					
				}
				
				
				
			}
			
			System.out.println(userModel.getPage());
			System.out.println(sql);
			
			return getEntitiesByNativeSQL(sql, userModel.getPage());
			
		}
		
		
		public void deleteUser(Integer userId) {
			String sql = "update user_customer set status = 0 where id = "+ userId;
			 super.getEntityByNativeSQL(sql);
			
		}
		
	

}
