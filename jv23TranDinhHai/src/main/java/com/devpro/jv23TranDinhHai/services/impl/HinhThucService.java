package com.devpro.jv23TranDinhHai.services.impl;

import org.springframework.stereotype.Service;

import com.devpro.jv23TranDinhHai.model.HinhThucEntity;
import com.devpro.jv23TranDinhHai.services.BaseService;

@Service
public class HinhThucService extends BaseService<HinhThucEntity>{
	
	@Override
	protected Class<HinhThucEntity> clazz() {
		// TODO Auto-generated method stub
		return HinhThucEntity.class;
	}

}
