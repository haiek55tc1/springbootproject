package com.devpro.jv23TranDinhHai.services.impl;

import org.springframework.stereotype.Service;

import com.devpro.jv23TranDinhHai.model.Categories;
import com.devpro.jv23TranDinhHai.services.BaseService;


@Service
public class CategoriesService extends BaseService<Categories>{
	@Override
	public Class<Categories> clazz() {
		return Categories.class;
	}

}
