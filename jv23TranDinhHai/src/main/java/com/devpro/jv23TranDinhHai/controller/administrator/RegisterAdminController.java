package com.devpro.jv23TranDinhHai.controller.administrator;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.devpro.jv23TranDinhHai.controller.BaseController;
import com.devpro.jv23TranDinhHai.model.Categories;
import com.devpro.jv23TranDinhHai.model.Role;
import com.devpro.jv23TranDinhHai.model.StatusEntity;
import com.devpro.jv23TranDinhHai.model.User;
import com.devpro.jv23TranDinhHai.services.impl.CategoriesService;
import com.devpro.jv23TranDinhHai.services.impl.RoleService;
import com.devpro.jv23TranDinhHai.services.impl.StatusService;
import com.devpro.jv23TranDinhHai.services.impl.UserService;
import com.fasterxml.jackson.annotation.JsonCreator.Mode;




@Controller
public class RegisterAdminController extends BaseController{
	
	
	
	@Autowired
	private RoleService roleService;	
	@Autowired 
	private UserService userService;
	@Autowired
	private CategoriesService categoriesSevice;
	
	@Autowired 
	private StatusService statusService;
	
	
	@RequestMapping( value = {"/admin/register"}, method = RequestMethod.GET)
	public String register(
			final Model model,
			final HttpServletRequest request,
			final HttpServletResponse response) throws IOException {
		
		
		User user = new User();
		model.addAttribute("user", user);
		
		
		
		List<Role> roles = roleService.findAll();
		model.addAttribute("roles", roles);
		
		List<Categories> categories = categoriesSevice.findAll();
		
		model.addAttribute("categories", categories);
		return "administrator/register";
		
	}
	
	
	@RequestMapping(value = {"/admin/register-post"}, method = RequestMethod.POST)
	public String registerPost(
			final Model model,
			final HttpServletRequest request,
			final HttpServletResponse response,
			@ModelAttribute("user") User user,
			@RequestParam("matTruoc") MultipartFile matTruoc,
			@RequestParam("matSau") MultipartFile matSau )
			throws IOException, IllegalStateException {
//		model.addAttribute("user", user);
//		userService.saveOrUpdate(user);
		
		User newUser = new User();
		
		List<StatusEntity> statusList = statusService.findAll();
		user.setStatusEntioitys(statusList.get(0));
		
		
		if (user.getId() == null || user.getId() <= 0) {
			
			userService.addUser(user, matTruoc, matSau);
		}else {
			userService.editUser(user, matTruoc, matSau);
		}
		
		
		
		return "redirect:/admin/user/list";
	}

}
