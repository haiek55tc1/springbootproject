package com.devpro.jv23TranDinhHai.controller.administrator;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.devpro.jv23TranDinhHai.controller.BaseController;
import com.devpro.jv23TranDinhHai.dto.UserSearch;
import com.devpro.jv23TranDinhHai.model.StatusEntity;
import com.devpro.jv23TranDinhHai.model.User;
import com.devpro.jv23TranDinhHai.services.PagerData;
import com.devpro.jv23TranDinhHai.services.impl.StatusService;
import com.devpro.jv23TranDinhHai.services.impl.UserService;



@Controller
public class UserController extends BaseController{
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private StatusService statusService;
	@RequestMapping(value = {"/admin/user/list"}, method = RequestMethod.GET)
	public String userList(
			final Model model,
			final HttpServletRequest request,
			final HttpServletResponse response
			 ) {
		List<StatusEntity> statusList = statusService.findAll();
		model.addAttribute("status", statusList);
		
//		List<User> users = userService.findAll();
//		model.addAttribute("users", users);
//		
		
		String keyword = request.getParameter("keyword");
		String statusId = request.getParameter("statusId");
		String currentPage = request.getParameter("currentPage");
		
		UserSearch userSearch = new UserSearch();
		
		userSearch.setKeyword(keyword);
		userSearch.setStatusId(statusId);
		userSearch.setCurrentPage(currentPage);
		
		
		
	PagerData<User> userS = userService.userSearch(userSearch);
		model.addAttribute("users", userS);
		
		model.addAttribute("userSeach", userSearch);
		
		return "administrator/userList";
		
	}

}
