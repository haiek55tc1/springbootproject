package com.devpro.jv23TranDinhHai.controller.customer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.devpro.jv23TranDinhHai.controller.BaseController;


@Controller
public class HomeController extends BaseController{
	
	
	@RequestMapping(value = {"/home"}, method = RequestMethod.GET)
	
	public String Home(final Model model,
				final HttpServletRequest request,
				final HttpServletResponse respopnse) {
		
		return "customer/Home";
		
	}
	

}
