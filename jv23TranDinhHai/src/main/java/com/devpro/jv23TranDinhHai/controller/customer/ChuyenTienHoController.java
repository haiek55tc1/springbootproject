package com.devpro.jv23TranDinhHai.controller.customer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.devpro.jv23TranDinhHai.controller.BaseController;
import com.devpro.jv23TranDinhHai.dto.FormNguoiNhanDTO;
import com.devpro.jv23TranDinhHai.model.ChuyenTienEntity;
import com.devpro.jv23TranDinhHai.model.HinhThucEntity;
import com.devpro.jv23TranDinhHai.model.User;
import com.devpro.jv23TranDinhHai.services.impl.ChuyenTienServiec;
import com.devpro.jv23TranDinhHai.services.impl.HinhThucService;
import com.devpro.jv23TranDinhHai.services.impl.UserService;

@Controller 
public class ChuyenTienHoController extends BaseController{
	
	@Autowired
	private HinhThucService hinhThucService;
	
	@Autowired
	private ChuyenTienServiec chuyenTienService;
	
	@Autowired
	private UserService userServiec;
	
	@RequestMapping(value = {"/chuyen-tien-ho"}, method = RequestMethod.GET)
	
	public String formNguoiGui(final Model model,
								final HttpServletRequest request,
								final HttpServletResponse response) {
		ChuyenTienEntity chuyenTien = new ChuyenTienEntity();
		
		model.addAttribute("formNguoiNhanDTO", chuyenTien);
		return "customer/formNguoiNhan";
	}
	
	
	@RequestMapping(value = {"/chuyen-tien-ho-post"}, method = RequestMethod.POST)
	public String nguoiGuiPost(final Model model,
								final HttpServletRequest request,
								final HttpServletResponse response,
								@ModelAttribute("formNguoiNhanDTO") ChuyenTienEntity chuyenTien) {
		
		
		HinhThucEntity hinhThucHo = hinhThucService.getById(1);
		chuyenTien.setHinhThucEntity(hinhThucHo);
		
		User user = userServiec.getById(21);
		chuyenTien.setUser(user);
		
		chuyenTienService.saveOrUpdate(chuyenTien);
		
		System.out.println(chuyenTien.getFullName());
		
		return "customer/formNguoiNhan";
	}
	
	
	

}
