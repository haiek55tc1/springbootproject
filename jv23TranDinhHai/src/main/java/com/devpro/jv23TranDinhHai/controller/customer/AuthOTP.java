package com.devpro.jv23TranDinhHai.controller.customer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class AuthOTP {

	
	
	@RequestMapping(value = {"/xac-thuc"}, method = RequestMethod.GET)
	public String xacThuc(final Model model,
					final HttpServletRequest request,
					final HttpServletResponse response) {
		return "customer/xacThucSDT";
	}
	
	
	@RequestMapping(value = {"/xac-thuc-post"}, method = RequestMethod.POST)
	public String optPost(final Model model,
					final HttpServletRequest request,
						final HttpServletResponse response) {
		String otp = request.getParameter("otp");
		System.out.println("ma: "+otp);
		return "customer/xacThucSDT";
	}
}
