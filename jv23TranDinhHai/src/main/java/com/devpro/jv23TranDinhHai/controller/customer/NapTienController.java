package com.devpro.jv23TranDinhHai.controller.customer;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.devpro.jv23TranDinhHai.controller.BaseController;

import com.devpro.jv23TranDinhHai.model.NapTienEntity;
import com.devpro.jv23TranDinhHai.model.StatusEntity;
import com.devpro.jv23TranDinhHai.model.User;
import com.devpro.jv23TranDinhHai.services.impl.NapTienService;
import com.devpro.jv23TranDinhHai.services.impl.StatusService;
import com.devpro.jv23TranDinhHai.services.impl.UserService;

@Controller
public class NapTienController extends BaseController{
	
	@Autowired 
	private NapTienService napTienService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private StatusService statusService;
	
	@RequestMapping(value = {"/nap-tien"}, method = RequestMethod.GET)
	private String napTien(final Model model,
							final HttpServletRequest request,
							final HttpServletResponse response) {
		return "customer/napTien";
	}
	
	@RequestMapping(value = {"/nap-tien-post"}, method = RequestMethod.POST)
	private ResponseEntity<Map<String, Object>> napTienPost(
				final Model model,
				final HttpServletRequest request,
				final HttpServletResponse response,
				final @RequestBody NapTienEntity napTienEntity){
		
		User user = userService.getById(21);
		StatusEntity statusE = statusService.getById(1);
		
		napTienEntity.setUsers(user);
		napTienEntity.setStatusEntitys(statusE);
		
		
		System.out.println("noi dung ck: "+ napTienEntity.getMoney());
		Map<String, Object> jsonResult = new HashMap<String, Object>();
		
		napTienService.saveOrUpdate(napTienEntity);

		return ResponseEntity.ok(jsonResult);
		
	}

}
