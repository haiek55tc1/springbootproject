package com.devpro.jv23TranDinhHai.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.devpro.jv23TranDinhHai.model.Categories;
import com.devpro.jv23TranDinhHai.model.Role;
import com.devpro.jv23TranDinhHai.services.impl.CategoriesService;
import com.devpro.jv23TranDinhHai.services.impl.RoleService;

public class BaseController {
	
	@Autowired
	private CategoriesService categoriesService;
	@Autowired
	private RoleService roleServiec;
	
	
	@ModelAttribute("categories")
	public List<Categories> categoriesList() {
		return  categoriesService.findAll();
	}
	
	
	@ModelAttribute("roles")
	public List<Role> roles(){
		return roleServiec.findAll();
	}

}
