package com.devpro.jv23TranDinhHai.controller.customer;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.devpro.jv23TranDinhHai.controller.BaseController;
import com.devpro.jv23TranDinhHai.dto.ChuyenTienNoiBoDTO;
import com.devpro.jv23TranDinhHai.model.ChuyenTienEntity;
import com.devpro.jv23TranDinhHai.model.HinhThucEntity;
import com.devpro.jv23TranDinhHai.model.User;
import com.devpro.jv23TranDinhHai.services.impl.ChuyenTienServiec;
import com.devpro.jv23TranDinhHai.services.impl.HinhThucService;
import com.devpro.jv23TranDinhHai.services.impl.UserService;



@Controller
public class ChuyenTienNoiBo extends BaseController{
	
	@Autowired
	private UserService userService;
	
	@Autowired 
	private ChuyenTienServiec chuyenTienService;
	
	@Autowired
	private HinhThucService hinhThucService;
	
	@RequestMapping(value = {"/chuyen-tien-noi-bo"}, method = RequestMethod.GET)
	public String chuyenTienNoiBo(final Model model,
									final HttpServletResponse response,
									final HttpServletRequest request) {
		ChuyenTienEntity chuyenTienNoiBoDTO = new ChuyenTienEntity();
		
		model.addAttribute("chuyenTienNoiBoDTO", chuyenTienNoiBoDTO);
		return "customer/chuyenTienNoiBo";
	}
	
	@RequestMapping(value = {"/chuyen-tien-noi-bo-post"}, method = RequestMethod.POST)
	
	public ResponseEntity<Map<String, Object>> chuyenTienNoiBoPost(
			final Model model,
			final HttpServletRequest request,
			final HttpServletResponse response,
			@RequestBody ChuyenTienEntity chuyenTienNoiBo){
		
		Map<String, Object> jsonResult = new HashMap<>();
		
		jsonResult.put("numberMoney", chuyenTienNoiBo.getMoney());
		jsonResult.put("phoneNumber", chuyenTienNoiBo.getPhoneNhan());
		User user =  userService.getById(21);
		chuyenTienNoiBo.setUser(user);
		
		HinhThucEntity hinhThuc = hinhThucService.getById(2);
		chuyenTienNoiBo.setHinhThucEntity(hinhThuc);
		
		chuyenTienService.saveOrUpdate(chuyenTienNoiBo);
		return ResponseEntity.ok(jsonResult);
		
	}
}



