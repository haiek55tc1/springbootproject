package com.devpro.jv23TranDinhHai.controller.customer;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.devpro.jv23TranDinhHai.controller.BaseController;
import com.devpro.jv23TranDinhHai.model.Categories;
import com.devpro.jv23TranDinhHai.model.Role;
import com.devpro.jv23TranDinhHai.model.StatusEntity;
import com.devpro.jv23TranDinhHai.model.User;
import com.devpro.jv23TranDinhHai.services.impl.CategoriesService;
import com.devpro.jv23TranDinhHai.services.impl.RoleService;
import com.devpro.jv23TranDinhHai.services.impl.StatusService;
import com.devpro.jv23TranDinhHai.services.impl.UserService;

@Controller
public class RegisterController extends BaseController{
	
	@Autowired
	private CategoriesService categoriesService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private  RoleService roleService;
	
	@Autowired
	private StatusService statusSeverice;
	
	@RequestMapping(value = {"/register"}, method = RequestMethod.GET)
	public String register(final Model model,
							final HttpServletRequest request,
							final HttpServletResponse response) {
		//khoi tao 1 product entity moi
		User user = new User();
		
		model.addAttribute("user", user);
		
		//Lay list categories ra
		List<Categories> categories = categoriesService.getEntitiesByNativeSQL("SELECT * FROM categories"); 
		
		///Day no xuong view
		
		model.addAttribute("categories", categories);
		
		return "customer/register";
	}
	
	@RequestMapping(value = {"/register-post"}, method = RequestMethod.POST)
	public String registerPost(final Model model,
									final HttpServletRequest request,
									final HttpServletResponse response,
									@ModelAttribute("user") User user,
									@RequestParam("matTruoc") MultipartFile  matTruoc,
									@RequestParam("matSau") MultipartFile matSau
									) throws IOException  {
		
		List<Role> listRole = roleService.findAll();
		user.setRoles(listRole.get(0));
		List<StatusEntity> listStatus = statusSeverice.findAll();
		user.setStatusEntioitys(listStatus.get(0));
		
		
		if (user.getId() == null || user.getId() <= 0) {
			userService.addUser(user, matTruoc, matSau);
		}else {
			
		}
	
		
		return "customer/register";
	}

}
