package com.devpro.jv23TranDinhHai.conf;

import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

@Configuration
public class MVCConf implements WebMvcConfigurer{
	
	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver bean = new InternalResourceViewResolver();
		
		
		bean.setViewClass(JstlView.class);
		
		
		bean.setPrefix("/WEB-INF/views/");
		
		bean.setSuffix(".jsp");
		return bean;
		
	}
	
	
	
	
	@Override 
	public void addResourceHandlers (final ResourceHandlerRegistry registy) {
		// Sử dụng addResurceHandler và addResourceLocation để cấu hình file
		
		registy.addResourceHandler("/css/**").addResourceLocations("classpath:/css/");
		//addResourceHandler("/css/**) <=> localhost:8080/abc/abc.css
		//addResourceLocations(""classpath:/css/) <=> src/main/resource/css
		registy.addResourceHandler("/js/**").addResourceLocations("classpath:/js/");
		registy.addResourceHandler("/imgs/**").addResourceLocations("classpath:/imgs/");
		registy.addResourceHandler("/upload/**").addResourceLocations("file:" + "/Users/apple/Desktop/upload/");
		
	}

}
