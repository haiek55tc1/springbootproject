package com.devpro.jv23TranDinhHai.dto;

public class FormNguoiNhanDTO {
	
	private String fullname;
	private int phonenumber;
	private int CCCDnumber;
	private String address;
	private int SecurityCode;
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public int getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(int phonenumber) {
		this.phonenumber = phonenumber;
	}
	public int getCCCDnumber() {
		return CCCDnumber;
	}
	public void setCCCDnumber(int cCCDnumber) {
		CCCDnumber = cCCDnumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getSecurityCode() {
		return SecurityCode;
	}
	public void setSecurityCode(int securityCode) {
		SecurityCode = securityCode;
	}
	
	
	
	

}
