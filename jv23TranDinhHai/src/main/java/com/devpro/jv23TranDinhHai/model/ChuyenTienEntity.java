package com.devpro.jv23TranDinhHai.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "chuyen_tien")
public class ChuyenTienEntity extends BaseEntity{
	
	@Column(name = "full_name", nullable = true)
	private String fullName;
	
	
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	@Column(name = "money", nullable = true)
	private BigDecimal money;
	
	

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	@Column(name = "phone_nhan", nullable = false)
	private int phoneNhan;
	
	@Column(name = "so_ho_so", nullable = true)
	private String soHoSo;
	
	@Column(name = "code", nullable = true)
	private int code;
	
	
	@Column(name = "address", nullable = true)
	private String address;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "hinh_thuc_id")
	private HinhThucEntity hinhThucEntity;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id")
	private User user = new User();

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getPhoneNhan() {
		return phoneNhan;
	}

	public void setPhoneNhan(int phoneNhan) {
		this.phoneNhan = phoneNhan;
	}



	public String getSoHoSo() {
		return soHoSo;
	}

	public void setSoHoSo(String soHoSo) {
		this.soHoSo = soHoSo;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public HinhThucEntity getHinhThucEntity() {
		return hinhThucEntity;
	}

	public void setHinhThucEntity(HinhThucEntity hinhThucEntity) {
		this.hinhThucEntity = hinhThucEntity;
	}
	
	
	
	
	

}
