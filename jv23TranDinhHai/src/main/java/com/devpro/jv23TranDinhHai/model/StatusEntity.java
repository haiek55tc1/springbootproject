package com.devpro.jv23TranDinhHai.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "status")
public class StatusEntity extends BaseEntity{
	@Column(name = "trang_thai", nullable = false)
	private String statusName;
	
	@OneToMany(fetch = FetchType.LAZY,
			cascade = CascadeType.ALL,
			mappedBy = "statusEntioitys")
	private Set<User> users = new HashSet<>();
	
	

	
	public void addUser(User user) {
		user.setStatusEntioitys(this);
		users.add(user);
	}
	
	public void removeUser(User user) {
		user.setStatusEntioitys(null);
		users.remove(user);
	}
	
	
	@OneToMany(fetch = FetchType.LAZY,
			cascade = CascadeType.ALL,
			mappedBy = "statusEntitys")
	
	private Set<NapTienEntity> napTienS = new HashSet<NapTienEntity>();
	 
	
	public void addNapTien(NapTienEntity napTien) {
		napTien.setStatusEntitys(this);
		napTienS.add(napTien);
	}
	
	public void removeNapTien(NapTienEntity napTien) {
		napTien.setStatusEntitys(null);
		napTienS.remove(napTien);
	}
	
	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}


	

}
