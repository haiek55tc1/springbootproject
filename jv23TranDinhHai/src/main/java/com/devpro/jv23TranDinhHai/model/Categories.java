package com.devpro.jv23TranDinhHai.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "categories")
public class Categories extends BaseEntity{

	@Column(name = "name", nullable = false)
	private String name;
	
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	@OneToMany(fetch = FetchType.LAZY,
			cascade = CascadeType.ALL,
			mappedBy = "categories")
	private Set<User> users = new HashSet<User>();
	
	public void addUser(User user) {
		user.setCatgories(this);
		users.add(user);
	}
	
	public void removeUser(User user) {
		user.setCatgories(null);
		users.remove(user);
	}

	public Set<User> getUsers() {
		return users;
	}


	public void setUsers(Set<User> users) {
		this.users = users;
	}
	
	
	
	
	
	
}
