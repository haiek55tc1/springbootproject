package com.devpro.jv23TranDinhHai.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "hinh_thuc")
public class HinhThucEntity extends BaseEntity{
	
	@Column(name = "hinh_thuc", nullable = false)
	private String hinhThuc;
	
	@OneToMany(fetch = FetchType.LAZY,
			cascade = CascadeType.ALL,
			mappedBy = "hinhThucEntity")
	
	private Set<ChuyenTienEntity> chuyenTienS = new HashSet<ChuyenTienEntity>();
	
	
	
	
	public void addChuyenTien(ChuyenTienEntity chuyenTien) {
		chuyenTien.setHinhThucEntity(this);
		chuyenTienS.add(chuyenTien);
	}
	
	public void removeChuyenTien(ChuyenTienEntity chuyenTien) {
		chuyenTien.setHinhThucEntity(null);
		chuyenTienS.remove(chuyenTien);
	}

	public String getHinhThuc() {
		return hinhThuc;
	}

	public void setHinhThuc(String hinhThuc) {
		this.hinhThuc = hinhThuc;
	}

	public Set<ChuyenTienEntity> getChuyenTienS() {
		return chuyenTienS;
	}

	public void setChuyenTienS(Set<ChuyenTienEntity> chuyenTienS) {
		this.chuyenTienS = chuyenTienS;
	}
	
	
	

}
