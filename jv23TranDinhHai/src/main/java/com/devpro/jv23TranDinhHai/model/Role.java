package com.devpro.jv23TranDinhHai.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

//Để có thẻ kết nối được với table của db ta cần 2 anotation là Entity và Table(name = "ten bang")

@Entity
@Table(name = "role")
public class Role extends BaseEntity{
	
	@Column(name = "role_name", nullable = false)
	private String roleName;
	
	@OneToMany(cascade = CascadeType.ALL,
			fetch = FetchType.LAZY,
			mappedBy = "roles"
			)
	private Set<User> users = new HashSet<User>();
	
	public void addUser(User user) {
		users.add(user);
		user.setRoles(this);
	}
	
	
	public void removeUser(User user) {
		users.remove(user);
		user.setRoles(null);
	}


	public String getRoleName() {
		return roleName;
	}


	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}


	public Set<User> getUsers() {
		return users;
	}


	public void setUsers(Set<User> users) {
		this.users = users;
	}
	
	
	

}
