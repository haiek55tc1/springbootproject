package com.devpro.jv23TranDinhHai.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



@Entity
@Table(name = "ho_so")
public class HoSoEntity extends BaseEntity{
	@Column(name = "mat_truoc", nullable = false)
	private String matTruoc;
	
	
	@Column(name = "mat_sau", nullable = false)
	private String matSau;
	
	
		
	

	public String getMatTruoc() {
		return matTruoc;
	}

	public void setMatTruoc(String matTruoc) {
		this.matTruoc = matTruoc;
	}

	public String getMatSau() {
		return matSau;
	}

	public void setMatSau(String matSau) {
		this.matSau = matSau;
	}

	@ManyToOne(cascade = CascadeType.ALL,
			fetch = FetchType.LAZY)
	@JoinColumn(name = "id_user")
	
	private User users;



	public User getUsers() {
		return users;
	}

	public void setUsers(User users) {
		this.users = users;
	}
	
	
	
	

}
