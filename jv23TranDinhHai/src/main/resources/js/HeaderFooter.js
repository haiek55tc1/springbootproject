const Header = () => {
  return `
    
        <div class = "header__container container">
            <div class = "header__logo">
            <img alt = "" src = "../imgs/logo.svg" />
            </div>
            <div class = "header_menu">
            <ul>
            <li class = "menu__active"><a href = "../layouts/Home.html">Trang chủ</a></li>
            <li>
            <a href = "#">Giao dịch</a>
            </li>
            <li><a href = "#">Về chúng tôi</a></li>
            </ul>
            
            </div>
            <div class = "header__select">
            <a href = "../layouts/login.html"><button class = "header__login">Đăng nhập</button></a>
            <a href = "../layouts/register.html"><button class = "header__register">Đăng ký</button></a>
            
            <select class = "header__language">
            <option>
                VN
            </option>
            <option>
            
               US
            </option>
            </select>
            </div>
        </div>
   
    `;
};

$("header").html(Header());

$(".header__login").on("click", function () {
  $(".header__login").addClass("user__active");
  $(".header__register").removeClass("user__active");
});

$(".header__register").on("click", function () {
  $(".header__register").addClass("user__active");
  $(".header__login").removeClass("user__active");
});

//

$(".header_menu > ul > li:nth-child(1)").on("click", function () {
  $(".header_menu > ul > li:nth-child(1)").addClass("menu__active");
  $(".header_menu > ul > li:nth-child(2)").removeClass("menu__active");
  $(".header_menu > ul > li:nth-child(3)").removeClass("menu__active");
});

$(".header_menu > ul > li:nth-child(2)").on("click", function () {
  $(".header_menu > ul > li:nth-child(2)").addClass("menu__active");
  $(".header_menu > ul > li:nth-child(1)").removeClass("menu__active");
  $(".header_menu > ul > li:nth-child(3)").removeClass("menu__active");
});

$(".header_menu > ul > li:nth-child(3)").on("click", function () {
  $(".header_menu > ul > li:nth-child(3)").addClass("menu__active");
  $(".header_menu > ul > li:nth-child(2)").removeClass("menu__active");
  $(".header_menu > ul > li:nth-child(1)").removeClass("menu__active");
});

const Footer = () => {
  return `
   <div class = "footer__container">
   <div class = "container">
   <div class = "footer__content">
       <div class = "footer__col1">
            <div class = "footer__logo"><img src = "../imgs/logo.svg"/></div>
            <div class = "footer__desc">
                <p>
                Amet minim mollit non deserunt
                 ullamco est sit aliqua dolor do
                  amet sint sintsintsintsintsint. 
                  Amet minim mollit non deserunt 
                  ullamco est sit aliqua dolor do 
                  amet sint sintsintsintsintsint. 
                
                </p>
            </div>

            <div class = "footter__col--menu">
                <ul>
                    <li><a href = "#">Giao dịch tiền</a></li>
                    <li><a href = "#">Chính sách</a></li>
                    <li><a href = "#">Hỗ trợ</a></li>
                </ul>
            </div>

            <div class = "footer__social">
                <img alt = "" src = "../imgs/fbFooter.svg" />
                <img alt = "" src = "../imgs/twFooterIcon.svg" />
                <img alt = "" src = "../imgs/ybFooter.svg" />
                <img alt = "" src = "../imgs/linFooter.svg" />
            </div>
       </div>
       <div class = "footer__col2">
        <h4>Về chúng tôi</h4>
        <ul>
        <li><a href = "#">Trang chủ</a></li>
        <li><a href = "#">Giao dịch</a></li>
        <li><a href = "#">Danh sách</a></li>
        <li><a href = "#">Bảo mật</a></li>
        <li><a href = "#">Hỗ trợ</a></li>
        </ul>
       
       </div>
       <div class = "footer__col3">
        <h4>FAQs</h4>
        <ul>
          <li><a href = "#">Câu hỏi</a></li>
          <li><a href = "#">Tư vấn</a></li>
          <li><a href = "#">Cộng đồng</a></li>
          <li><a href = "#">Hội nghị</a></li>
          <li><a href = "#">Group</a></li>
        
        </ul>
       </div>
       <div class = "footer__col4">
       <h4>Liên hệ</h4>
        <div class = "location1Footer">
          <img alt = "" src = "../imgs/location1Footer.svg" />
          <p>Số 175, đường Quyết Thắng, quận Hà Đông</p>
        </div>
        <div class = "location2Footer">
        <img alt = "" src = "../imgs/location2Footer.svg" />
        <div>
          <p>Cơ sở chính: </p>
          <p>- Số 255d6, đường  Hai Bà Trưng, quận Hoàn Kiếm</p>
          <p>- Số 255d6, đường Hai Bà Trưng, quận Hoàn Kiếm</p>
        </div>
        
        </div>
        <div class = "location3Footer">
        <img alt = "" src = "../imgs/location3Footer.svg" />
        <div>
        <p>Các chi nhánh: </p>
        <p>- Số 255d6, đường  Hai Bà Trưng, quận Hoàn Kiếm</p>
        <p>- Số 255d6, đường Hai Bà Trưng, quận Hoàn Kiếm</p>
      </div>
        </div>
       
       </div>
   </div>
   <div class = "footer__note">
   <p>@2021 - 2022 CTTK powred by member DEVPRO</p>
   
   </div>

</div>
   
   </div>
    
    
    `;
};

$("footer").html(Footer());
