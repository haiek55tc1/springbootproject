

const Header = () => {
  return `
    
        <div class = "header__container container">
            <div class = "header__logo">
            <img alt = "" src = "../imgs/logo.svg" />
            </div>
            <div class = "header_menu">
            <ul>
            <li class = "menu__active"><a href = "../layouts/Home.html">Trang chủ</a></li>
            <li class="dropdown show">
            <a class="btn btn-secondary dropdown-toggle bg-transparent border-0 text-dark outline-none" 
              href="#" 
              role="button" 
              id="dropdownMenuLink" 
              data-toggle="dropdown" 
              aria-haspopup="true" 
              aria-expanded="false"
             >Giao dịch</a>
            
             <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
              <a class="dropdown-item" href="../layouts/chuyenTienNoiBo.html">Chuyển tiền nội bộ</a>
               <a class="dropdown-item" href="../layouts/formNguoiGui.html">Chuyển tiền hộ</a>
               <a class="dropdown-item" href="../layouts/napTien.html">Nạp tiền vào tài khoản</a>
               <a class="dropdown-item" href="#">Gửi tiết kiệm</a>
               <a class="dropdown-item" href="#">Vay vốn</a>


  </div>
            
            </li>
            <li><a href = "#">Về chúng tôi</a></li>
            </ul>
            
            </div>
            <div class = "header__select d-flex align-items-center">
           <div class = "header__user d-flex align-items-center">
              <div class = "">
              <svg width="24" height="27" viewBox="0 0 24 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <g clip-path="url(#clip0_2477_1349)">
                  <path d="M12 13.5C13.8186 13.5 15.5628 12.7888 16.8487 11.523C18.1347 10.2571 18.8571 8.54021 18.8571 6.75C18.8571 4.95979 18.1347 3.2429 16.8487 1.97703C15.5628 0.711159 13.8186 0 12 0C10.1814 0 8.43723 0.711159 7.15127 1.97703C5.8653 3.2429 5.14286 4.95979 5.14286 6.75C5.14286 8.54021 5.8653 10.2571 7.15127 11.523C8.43723 12.7888 10.1814 13.5 12 13.5ZM9.55179 16.0312C4.275 16.0312 0 20.2395 0 25.4338C0 26.2986 0.7125 27 1.59107 27H22.4089C23.2875 27 24 26.2986 24 25.4338C24 20.2395 19.725 16.0312 14.4482 16.0312H9.55179Z" fill="black"/>
                  </g>
                  <defs>
                  <clipPath id="clip0_2477_1349">
                  <rect width="24" height="27" fill="white"/>
                  </clipPath>
                  </defs>
              </svg>


              </div>

              <div class = "header__user--name mr-3 dropdown show">
                  <a class="btn btn-secondary dropdown-toggle" 
                   href="#" role="button" id="dropdownMenuLink"
                     data-toggle="dropdown" aria-haspopup="true" 
                      aria-expanded="false">
                         Hải
                   </a>

                  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                  <a class="dropdown-item" href="../layouts/profile.html">Hồ sơ</a>
                  <a class="dropdown-item" href="../layouts/profile.html">Nạp tiền</a>
                  <a class="dropdown-item" href="../layouts/lichSuChuyenTien.html">Lịch sử giao dịch</a>
                   <a class="dropdown-item" href="#">Đăng xuất</a>
                    </div>           
              </div>

              <div class = "header__user--so-du">10000000</div>
           
           
           </div>
            
            <select class = "header__language">
            <option>
                VND
            </option>
            <option>
            
               USA
            </option>
            </select>
            </div>
        </div>
   
    `;
};

//$("header").html(Header());

$(".header__login").on("click", function () {
  $(".header__login").addClass("user__active");
  $(".header__register").removeClass("user__active");
});

$(".header__register").on("click", function () {
  $(".header__register").addClass("user__active");
  $(".header__login").removeClass("user__active");
});

//

$(".header_menu > ul > li:nth-child(1)").on("click", function () {
  $(".header_menu > ul > li:nth-child(1)").addClass("menu__active");
  $(".header_menu > ul > li:nth-child(2)").removeClass("menu__active");
  $(".header_menu > ul > li:nth-child(3)").removeClass("menu__active");
});

$(".header_menu > ul > li:nth-child(2)").on("click", function () {
  $(".header_menu > ul > li:nth-child(2)").addClass("menu__active");
  $(".header_menu > ul > li:nth-child(1)").removeClass("menu__active");
  $(".header_menu > ul > li:nth-child(3)").removeClass("menu__active");
});

$(".header_menu > ul > li:nth-child(3)").on("click", function () {
  $(".header_menu > ul > li:nth-child(3)").addClass("menu__active");
  $(".header_menu > ul > li:nth-child(2)").removeClass("menu__active");
  $(".header_menu > ul > li:nth-child(1)").removeClass("menu__active");
});

const Footer = () => {
  return `
   <div class = "footer__container">
   <div class = "container">
   <div class = "footer__content">
       <div class = "footer__col1">
            <div class = "footer__logo"><img src = "../imgs/logo.svg"/></div>
            <div class = "footer__desc">
                <p>
                Amet minim mollit non deserunt
                 ullamco est sit aliqua dolor do
                  amet sint sintsintsintsintsint. 
                  Amet minim mollit non deserunt 
                  ullamco est sit aliqua dolor do 
                  amet sint sintsintsintsintsint. 
                
                </p>
            </div>

            <div class = "footter__col--menu">
                <ul>
                    <li><a href = "#">Giao dịch tiền</a></li>
                    <li><a href = "#">Chính sách</a></li>
                    <li><a href = "#">Hỗ trợ</a></li>
                </ul>
            </div>

            <div class = "footer__social">
                <img alt = "" src = "../imgs/fbFooter.svg" />
                <img alt = "" src = "../imgs/twFooterIcon.svg" />
                <img alt = "" src = "../imgs/ybFooter.svg" />
                <img alt = "" src = "../imgs/linFooter.svg" />
            </div>
       </div>
       <div class = "footer__col2">
        <h4>Về chúng tôi</h4>
        <ul>
        <li><a href = "#">Trang chủ</a></li>
        <li><a href = "#">Giao dịch</a></li>
        <li><a href = "#">Danh sách</a></li>
        <li><a href = "#">Bảo mật</a></li>
        <li><a href = "#">Hỗ trợ</a></li>
        </ul>
       
       </div>
       <div class = "footer__col3">
        <h4>FAQs</h4>
        <ul>
          <li><a href = "#">Câu hỏi</a></li>
          <li><a href = "#">Tư vấn</a></li>
          <li><a href = "#">Cộng đồng</a></li>
          <li><a href = "#">Hội nghị</a></li>
          <li><a href = "#">Group</a></li>
        
        </ul>
       </div>
       <div class = "footer__col4">
       <h4>Liên hệ</h4>
        <div class = "location1Footer">
          <img alt = "" src = "../imgs/location1Footer.svg" />
          <p>Số 175, đường Quyết Thắng, quận Hà Đông</p>
        </div>
        <div class = "location2Footer">
        <img alt = "" src = "../imgs/location2Footer.svg" />
        <div>
          <p>Cơ sở chính: </p>
          <p>- Số 255d6, đường  Hai Bà Trưng, quận Hoàn Kiếm</p>
          <p>- Số 255d6, đường Hai Bà Trưng, quận Hoàn Kiếm</p>
        </div>
        
        </div>
        <div class = "location3Footer">
        <img alt = "" src = "../imgs/location3Footer.svg" />
        <div>
        <p>Các chi nhánh: </p>
        <p>- Số 255d6, đường  Hai Bà Trưng, quận Hoàn Kiếm</p>
        <p>- Số 255d6, đường Hai Bà Trưng, quận Hoàn Kiếm</p>
      </div>
        </div>
       
       </div>
   </div>
   <div class = "footer__note">
   <p>@2021 - 2022 CTTK powred by member DEVPRO</p>
   
   </div>

</div>
   
   </div>
    
    
    `;
};

$("footer").html(Footer());
