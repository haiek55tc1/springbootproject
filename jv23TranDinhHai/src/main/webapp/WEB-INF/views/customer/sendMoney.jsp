<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <jsp:include page = "/WEB-INF/views/common/variables.jsp"></jsp:include>
    <jsp:include page="/WEB-INF/views/common/css.jsp"></jsp:include>
    <title>chá»n phÆ°Æ¡ng thá»©c nháº­n tiá»n tiá»n</title>
</head>
<body>
    <header>

        <jsp:include page = "/WEB-INF/views/common/header.jsp"></jsp:include>
    </header>
    <main >
       
     <div class="mt-100 mb-100" >
        <div class="container box-shadow-col rounded-24  p-4" style="background: rgba(255, 251, 146, 0.07);">
            <div class=" d-flex align-items-center">
                <div class="w-75">
                    <div>
                        <p class="m-0">Số tiền chuyển</p>
                        <input type="number" class="money_transfer w-100 bg-transparent outline-none border-bottom-input">
                        <p class="m-0 money__transfer--warning text-warning"></p>

                    </div>

                    <div class="mt-3">
                        <p class="m-0">Số tiền nhận</p>
                        <p class="money_get w-100 bg-transparent outline-none border-bottom-input pt-2">0</p>
                        <p class="m-0"></p>
                    </div>

                    <div class="d-flex justify-content-between mt-2">
                        <p>Lệ phí</p>
                        <p class="text-normal font-weight-light"><span class="le_phi">1.2</span>%</p>
                    </div>
                </div>
                <div class="w-25 rounded-24 box-shadow-col pl-5 ml-4 py-5" style="background-color: #FFFFFF;">
                    <div>
                        <input type="radio" id="van_phong" name="nhan_tien" checked value="nhan tai van phong">
                        <label for="van_phong">Nhận tại văn phòng</label>
                    </div>

                    <div>
                        <input type="radio" id="tan_tay" name="nhan_tien" value = "giao tien tan tay">
                        <label for="tan_tay">Giao tiền tận tay</label>
                    </div>
                </div>
            </div>
            <a href="${base}/xac-thuc" class="button d-flex justify-content-end mt-4 ">
                <button onClick = "chuyenTienHo('${base}')" class="send__money--btn bg-primary-color border-0 rounded-24 px-4 py-2 outline-none cursor-pointer">Tiáº¿p tá»¥c</button>
            </a>
        </div>
     </div>
    </main>
    <footer></footer>

    <jsp:include page="/WEB-INF/views/common/js.jsp"></jsp:include>
</body>
</html>