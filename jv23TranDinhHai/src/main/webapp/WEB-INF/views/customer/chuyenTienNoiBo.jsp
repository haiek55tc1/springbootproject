<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <jsp:include page = "/WEB-INF/views/common/variables.jsp"></jsp:include>
   <jsp:include page="/WEB-INF/views/common/css.jsp"></jsp:include>
    <title>Chuyển tiền nội bộ</title>
</head>
<body>
    <header>
      <jsp:include page = "/WEB-INF/views/common/header.jsp"></jsp:include>
    </header>
    <main>
            <h4 class="h4 font-weight-bold text-center mt-100 ">Chuyển tiện nội bộ</h4>

            <div class="register mb-100">
                <div class="container">
                    <form class="ml-100 mr-100" action="">
                      <!--  -->
                      <div class="password mt-4">
                        <p class="h6 font-weight-light">Nhập số tiền</p>
                        <input name = "money" class=" so_tien_chuyen w-100 rounded border-input-reg py-2 pl-3" type="number" name="money" placeholder="10000000">
                        <p class="transfer__noi-bo--warning text-warning m-0 mt-2"></p>
                    </div>

                        <!--  -->
                        <div class="email mt-2">
                           <div class="d-flex align-items-center justify-content-between mb-2">
                            <p class="h6 font-weight-light">Nhập số điện thoại</p>
   							
								<button onClick = "checkPhone('${base}')" type="button" style="font-size: 12px;" class="check__noi-bo--btn bg-transparent border-input-reg rounded-24 px-2 py-1">
                            Kiểm tra
                            </button>
   							
   				
                           </div>
                            <input name = "phoneNumber" class="phone__number--noi-bo w-100 rounded border-input-reg py-2 pl-3" type="number" name="phoneNumber" placeholder="Số điện thoại">
                        <p class="phone-number__noi-bo--warning text-warning m-0 mt-2"></p>

                        </div>

                          <!--  -->
                          <div class="password mt-4">
                            <p  class="h6 font-weight-light">Họ và tên</p>
                            <input value = "" class="fullname__chuyen-tien w-100 border-bottom-input py-2"></input>
                        </div>
                        <!-- button -->

                        <a class="noi__bo d-flex justify-content-center mt-4">
                            <button onClick = "chuyenTienNoiBo('${base}')" type="button" class="noi__bo--btn bg-primary-color border-0 text-white text-center rounded-24 w-25 py-2 cursor-pointer outline-none">Tiếp tục</button>
                        </a>
                    </form>


                    
                </div>
            </div>






    </main>
    <footer></footer>
    <jsp:include page="/WEB-INF/views/common/js.jsp"></jsp:include>
    
</body>
</html>