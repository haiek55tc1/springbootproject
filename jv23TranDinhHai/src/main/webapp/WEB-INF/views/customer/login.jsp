<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <jsp:include page = "/WEB-INF/views/common/variables.jsp"></jsp:include>
    <jsp:include page="/WEB-INF/views/common/css.jsp"></jsp:include>


    <title>ÄÄng nháº­p</title>
</head>
<body>
    <header>
    
    
      <jsp:include page = "/WEB-INF/views/common/header.jsp"></jsp:include>
    
    </header>
    <main>
            <h4 class="h4 font-weight-bold text-center mt-100 ">ÄÄng nháº­p tÃ i khoáº£n <span class="text-primary-color">"chuyá»n tiá»n tiáº¿t kiá»m"</span> cá»§a báº¡n</h4>

            <div class="register mb-100">
                <div class="container">
                    <form class="ml-100 mr-100" action="">
                    

                        <!--  -->
                        <div class="email mt-4">
                            <p class="h6 font-weight-light">Nháº­p sá» Äiá»n thoáº¡i</p>
                            <input class="phone-number__login  w-100 rounded border-input-reg py-2 pl-3" type="number" name="phoneNumber" placeholder="Sá» Äiá»n thoáº¡i">
                            <p class="phonee-number__warning m-0 pt-2 text-warning"></p>
                        </div>

                          <!--  -->
                          <div class="password mt-2">
                            <p class="h6 font-weight-light">Nháº­p máº­t kháº©u</p>
                            <input class="pass__login  w-100 rounded border-input-reg py-2 pl-3" type="password" name="phoneNumber" placeholder="***************">
                        </div>
                        <!-- button -->

                        <a href = "${base }/home" class="d-flex justify-content-center mt-4">
                            <button onClick = "loginPost('${base}')" type="button" class="login__btn bg-primary-color border-0 text-white text-center rounded-24 w-25 py-2 cursor-pointer outline-none">Tiáº¿p tá»¥c</button>
                        </a>
                    </form>


                    <div class="register__sub  mt-4 ml-100 mr-100">
                        <div class="d-flex justify-content-center"><svg width="585" height="20" viewBox="0 0 585 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g opacity="0.25">
                            <line x1="0.5" y1="9.5" x2="278.5" y2="9.5" stroke="black"/>
                            <path d="M289.644 16.096C292.844 16.096 295.308 13.776 295.308 10.32C295.308 6.848 292.844 4.528 289.644 4.528C286.236 4.528 283.932 7.04 283.932 10.32C283.932 13.6 286.236 16.096 289.644 16.096ZM289.644 14.768C287.1 14.768 285.372 12.784 285.372 10.32C285.372 7.856 287.1 5.856 289.644 5.856C291.996 5.856 293.852 7.68 293.852 10.32C293.852 12.928 291.996 14.768 289.644 14.768ZM301.381 8.112C300.325 8.112 299.413 8.656 298.837 9.584H298.821V8.208H297.525V16H298.821V12.24C298.821 10.672 299.717 9.536 301.189 9.536C301.317 9.536 301.525 9.552 301.701 9.568V8.128C301.621 8.112 301.509 8.112 301.381 8.112Z" fill="black"/>
                            <line x1="306.5" y1="9.5" x2="584.5" y2="9.5" stroke="black"/>
                            </g>
                            </svg>
                        </div>

                        <div class="register__socila d-flex justify-content-center align-items-center mt-3">
                            <div class="register__gg pr-1"><svg width="174" height="48" viewBox="0 0 174 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect x="0.5" width="173.333" height="48" rx="8" fill="#EEEEEE"/>
                                <g clip-path="url(#clip0_1423_5877)">
                                <path d="M101.957 21.2184L88.9049 21.2178C88.3285 21.2178 87.8613 21.6849 87.8613 22.2613V26.431C87.8613 27.0072 88.3285 27.4745 88.9048 27.4745H96.2552C95.4503 29.5633 93.9481 31.3126 92.0315 32.4241L95.1656 37.8496C100.193 34.942 103.166 29.8401 103.166 24.129C103.166 23.3158 103.106 22.7345 102.986 22.08C102.895 21.5826 102.463 21.2184 101.957 21.2184Z" fill="#167EE6"/>
                                <path d="M87.1657 33.7389C83.5686 33.7389 80.4283 31.7735 78.7418 28.8652L73.3164 31.9924C76.0773 36.7775 81.2495 39.9998 87.1657 39.9998C90.068 39.9998 92.8066 39.2184 95.1657 37.8565V37.8491L92.0315 32.4235C90.5979 33.255 88.9389 33.7389 87.1657 33.7389Z" fill="#12B347"/>
                                <path d="M95.166 37.8569V37.8495L92.0318 32.4238C90.5982 33.2553 88.9393 33.7393 87.166 33.7393V40.0001C90.0683 40.0001 92.807 39.2187 95.166 37.8569Z" fill="#0F993E"/>
                                <path d="M77.4269 24.0004C77.4269 22.2273 77.9108 20.5685 78.7421 19.1349L73.3167 16.0078C71.9474 18.3595 71.166 21.0906 71.166 24.0004C71.166 26.9101 71.9474 29.6413 73.3167 31.9929L78.7421 28.8658C77.9108 27.4322 77.4269 25.7734 77.4269 24.0004Z" fill="#FFD500"/>
                                <path d="M87.1657 14.2609C89.5114 14.2609 91.666 15.0944 93.349 16.4808C93.7642 16.8228 94.3676 16.7981 94.7479 16.4178L97.7023 13.4634C98.1338 13.0319 98.103 12.3256 97.6421 11.9258C94.8224 9.47956 91.1537 8 87.1657 8C81.2495 8 76.0773 11.2223 73.3164 16.0074L78.7418 19.1346C80.4283 16.2262 83.5686 14.2609 87.1657 14.2609Z" fill="#FF4B26"/>
                                <path d="M93.3493 16.4808C93.7645 16.8228 94.368 16.7981 94.7482 16.4178L97.7026 13.4634C98.134 13.0319 98.1033 12.3256 97.6424 11.9258C94.8227 9.4795 91.154 8 87.166 8V14.2609C89.5116 14.2609 91.6663 15.0944 93.3493 16.4808Z" fill="#D93F21"/>
                                </g>
                                <defs>
                                <clipPath id="clip0_1423_5877">
                                <rect width="32" height="32" fill="white" transform="translate(71.166 8)"/>
                                </clipPath>
                                </defs>
                                </svg>
                            </div>

                            <div class="register__fb px-1">
                                <svg width="175" height="48" viewBox="0 0 175 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <rect x="0.833984" width="173.333" height="48" rx="8" fill="#EEEEEE"/>
                                    <g clip-path="url(#clip0_1423_5886)">
                                    <path d="M103.5 24C103.5 31.9862 97.6488 38.6056 90 39.8056V28.625H93.7281L94.4375 24H90V20.9987C90 19.7331 90.62 18.5 92.6075 18.5H94.625V14.5625C94.625 14.5625 92.7938 14.25 91.0431 14.25C87.3888 14.25 85 16.465 85 20.475V24H80.9375V28.625H85V39.8056C77.3513 38.6056 71.5 31.9862 71.5 24C71.5 15.1637 78.6637 8 87.5 8C96.3363 8 103.5 15.1637 103.5 24Z" fill="#1877F2"/>
                                    <path d="M93.7281 28.625L94.4375 24H90V20.9987C90 19.7334 90.6199 18.5 92.6074 18.5H94.625V14.5625C94.625 14.5625 92.794 14.25 91.0434 14.25C87.3887 14.25 85 16.465 85 20.475V24H80.9375V28.625H85V39.8056C85.8146 39.9334 86.6495 40 87.5 40C88.3505 40 89.1854 39.9334 90 39.8056V28.625H93.7281Z" fill="white"/>
                                    </g>
                                    <defs>
                                    <clipPath id="clip0_1423_5886">
                                    <rect width="32" height="32" fill="white" transform="translate(71.5 8)"/>
                                    </clipPath>
                                    </defs>
                                    </svg>
                                    
                            </div>

                            <div class="register__ap pl-1">
                                <svg width="174" height="48" viewBox="0 0 174 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <rect x="0.166016" width="173.333" height="48" rx="8" fill="#EEEEEE"/>
                                    <path d="M92.9916 8C93.0661 8 93.1406 8 93.2193 8C93.4019 10.2567 92.5406 11.9429 91.4937 13.164C90.4665 14.3767 89.0599 15.5528 86.785 15.3743C86.6332 13.15 87.496 11.5888 88.5414 10.3705C89.511 9.23515 91.2885 8.22483 92.9916 8Z" fill="black"/>
                                    <path d="M99.8775 31.4892C99.8775 31.5116 99.8775 31.5313 99.8775 31.5524C99.2381 33.4887 98.3262 35.1482 97.2133 36.6883C96.1974 38.0864 94.9524 39.968 92.7294 39.968C90.8085 39.968 89.5326 38.7328 87.564 38.6991C85.4815 38.6654 84.3363 39.7319 82.4323 40.0003C82.2145 40.0003 81.9967 40.0003 81.7831 40.0003C80.3849 39.7979 79.2566 38.6907 78.4346 37.693C76.0106 34.7449 74.1375 30.9369 73.7891 26.0638C73.7891 25.586 73.7891 25.1097 73.7891 24.6319C73.9366 21.1443 75.6312 18.3086 77.8837 16.9344C79.0725 16.2037 80.7067 15.5812 82.5264 15.8594C83.3063 15.9803 84.103 16.2473 84.8014 16.5114C85.4632 16.7658 86.2909 17.2168 87.075 17.1929C87.6061 17.1775 88.1345 16.9007 88.6698 16.7053C90.238 16.1391 91.7753 15.4899 93.8015 15.7948C96.2367 16.1629 97.9651 17.2449 99.033 18.9143C96.973 20.2253 95.3444 22.201 95.6226 25.5748C95.8699 28.6395 97.6517 30.4325 99.8775 31.4892Z" fill="black"/>
                                    </svg>
                                    
                            </div>
                        </div>

                        <div class="register__agree text-center mt-2">
                            <p class="font-weight-light">Báº¡n cÃ³ Äá»ng Ã½ vá»i cÃ¡c <span><a href="">Äiá»u khoáº£n sá»­ dá»¥ng</a></span> vÃ  <span><a href="">quyá»n riÃªng tÆ°</a></span></p>
                        </div>

                        <div class="register__accont text-center ">
                            <p class="font-weight-light">Báº¡n chÆ°a cÃ³ tÃ i khoáº£n? <span><a href="">Register</a></span></p>
                        </div>
                    </div>
                </div>
            </div>






    </main>
    <footer></footer>



    
    <jsp:include page="/WEB-INF/views/common/js.jsp"></jsp:include>
</body>
</html>