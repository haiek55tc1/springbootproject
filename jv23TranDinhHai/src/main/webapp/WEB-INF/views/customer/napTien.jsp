<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <jsp:include page = "/WEB-INF/views/common/variables.jsp"></jsp:include>
    <jsp:include page="/WEB-INF/views/common/css.jsp"></jsp:include>

    <title>Nạp tiền</title>
</head>
<body>
    <header>
        <jsp:include page = "/WEB-INF/views/common/header.jsp"></jsp:include>



    </header>
    <main >
       
     <div class="mt-100 mb-100" >
        <div class="container box-shadow-col rounded-24  p-4" style="background: rgba(255, 251, 146, 0.07);">
            <div class=" d-flex align-items-center">
                <div class="w-75">
                    <div>
                        <p class="m-0">Số tiền nạp</p>
                        <input type="number" class="number__money w-100 bg-transparent outline-none border-bottom-input">
                        <p class="m-0 number__money--warning text-warning"></p>

                    </div>

                    <div class="mt-3">
                        <p class="m-0">Nội dung chuyển khoản</p>
                        <p class="transfer__content w-100 bg-transparent outline-none border-bottom-input pt-2">
                        0376583710</p>
                        
                    </div>

                   
                </div>
                <div class="w-25 rounded-24 box-shadow-col p-3 ml-4 ">
                   <p class="m-0 pb-2">Quét mã chuyển khoản</p>
                   <div class="h-100 w-100">
                       <img class="object-fit-cover w-100" src="${base}/imgs/QR.jpeg" alt="">
                   </div>
                </div>
            </div>
            <a class=" d-flex justify-content-end mt-4 ">
                <button onClick = "napTienPost('${base}')" class="send__request--btn bg-primary-color border-0 rounded-24 px-4 py-2 outline-none cursor-pointer">
                    Gửi yêu cầu</button>
            </a>
        </div>
     </div>
    </main>
    <footer></footer>

    
    <jsp:include page="/WEB-INF/views/common/js.jsp"></jsp:include>
</body>
</html>