<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>




<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <jsp:include page = "/WEB-INF/views/common/variables.jsp"></jsp:include>
    <jsp:include page="/WEB-INF/views/common/css.jsp"></jsp:include>
    <title>Hồ sơ cá nhân</title>
  </head>
  <body>
    <header>
      <jsp:include page = "/WEB-INF/views/common/header.jsp"></jsp:include>

    </header>
    <main class="pt-100 pb-100" style="background: rgba(255, 251, 146, 0.07)">
      <div class="container box-shadow-col rounded-24 p-4">
        <!-- update avt -->
        <div class="update__avt d-flex align-items-center cursor-pointer">
          <input type="file" class="d-none" id="avt" />
          <label for="avt" class="d-flex align-items-center">
            <div class="d-flex align-items-center">
              <svg
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M23 19C23 19.5304 22.7893 20.0391 22.4142 20.4142C22.0391 20.7893 21.5304 21 21 21H3C2.46957 21 1.96086 20.7893 1.58579 20.4142C1.21071 20.0391 1 19.5304 1 19V8C1 7.46957 1.21071 6.96086 1.58579 6.58579C1.96086 6.21071 2.46957 6 3 6H7L9 3H15L17 6H21C21.5304 6 22.0391 6.21071 22.4142 6.58579C22.7893 6.96086 23 7.46957 23 8V19Z"
                  stroke="black"
                  stroke-opacity="0.25"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
                <path
                  d="M12 17C14.2091 17 16 15.2091 16 13C16 10.7909 14.2091 9 12 9C9.79086 9 8 10.7909 8 13C8 15.2091 9.79086 17 12 17Z"
                  stroke="black"
                  stroke-opacity="0.25"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
              </svg>
            </div>
            <p class="m-0 font-weight-light ml-4">Cập nhật ảnh đại diện</p>
          </label>
        </div>

        <!-- content 1-->
        <div class="d-flex align-items-center justify-content-between mt-3">
          <!--  -->
          <div class="d-flex align-items-center">
            <div class="icon d-flex align-items-center mr-3">
              <svg
                width="48"
                height="49"
                viewBox="0 0 48 49"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <rect y="0.5" width="48" height="48" rx="24" fill="#414042" />
                <path
                  d="M33.9994 29.4201V32.4201C34.0006 32.6986 33.9435 32.9743 33.832 33.2294C33.7204 33.4846 33.5567 33.7137 33.3515 33.902C33.1463 34.0902 32.904 34.2336 32.6402 34.3228C32.3764 34.412 32.0968 34.4452 31.8194 34.4201C28.7423 34.0857 25.7864 33.0342 23.1894 31.3501C20.7733 29.8148 18.7248 27.7663 17.1894 25.3501C15.4994 22.7413 14.4477 19.7711 14.1194 16.6801C14.0945 16.4036 14.1273 16.1249 14.2159 15.8617C14.3046 15.5986 14.447 15.3568 14.6342 15.1517C14.8214 14.9466 15.0492 14.7828 15.3032 14.6706C15.5572 14.5584 15.8318 14.5004 16.1094 14.5001H19.1094C19.5948 14.4953 20.0652 14.6672 20.4332 14.9836C20.8012 15.3001 21.0415 15.7395 21.1094 16.2201C21.2361 17.1802 21.4709 18.1228 21.8094 19.0301C21.944 19.388 21.9731 19.777 21.8934 20.151C21.8136 20.5249 21.6283 20.8682 21.3594 21.1401L20.0894 22.4101C21.513 24.9136 23.5859 26.9865 26.0894 28.4101L27.3594 27.1401C27.6313 26.8712 27.9746 26.6859 28.3486 26.6062C28.7225 26.5264 29.1115 26.5556 29.4694 26.6901C30.3767 27.0286 31.3194 27.2635 32.2794 27.3901C32.7652 27.4586 33.2088 27.7033 33.526 28.0776C33.8431 28.4519 34.0116 28.9297 33.9994 29.4201Z"
                  stroke="white"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
              </svg>
            </div>
            <div>
              <p class="profile__title m-0 font-weight-bold fs-14">
                Sá» di Äá»ng
              </p>
              <p
                class="profile__inf profile__inf--phone-number m-0 font-weight-light fs-12"
              >
                0376583710
              </p>
            </div>
          </div>

          <!--  -->
          <div class="icon d-flex align-items-center">
            <svg
              width="24"
              height="25"
              viewBox="0 0 24 25"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M12 20.5H21"
                stroke="black"
                stroke-width="2"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
              <path
                d="M16.5 4.00023C16.8978 3.6024 17.4374 3.37891 18 3.37891C18.2786 3.37891 18.5544 3.43378 18.8118 3.54038C19.0692 3.64699 19.303 3.80324 19.5 4.00023C19.697 4.19721 19.8532 4.43106 19.9598 4.68843C20.0665 4.9458 20.1213 5.22165 20.1213 5.50023C20.1213 5.7788 20.0665 6.05465 19.9598 6.31202C19.8532 6.56939 19.697 6.80324 19.5 7.00023L7 19.5002L3 20.5002L4 16.5002L16.5 4.00023Z"
                stroke="black"
                stroke-width="2"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
            </svg>
          </div>
        </div>


         <!-- content 2-->
         <div class="d-flex align-items-center justify-content-between mt-3">
            <!--  -->
            <div class="d-flex align-items-center">
              <div class="icon d-flex align-items-center mr-3">
                <svg width="48" height="49" viewBox="0 0 48 49" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect y="0.5" width="48" height="48" rx="24" fill="#414042"/>
                    <path d="M16 16.5H32C33.1 16.5 34 17.4 34 18.5V30.5C34 31.6 33.1 32.5 32 32.5H16C14.9 32.5 14 31.6 14 30.5V18.5C14 17.4 14.9 16.5 16 16.5Z" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M34 18.5L24 25.5L14 18.5" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                    
              </div>
              <div>
                <p class="profile__title m-0 font-weight-bold fs-14">
                  Email
                </p>
                <p
                  class="profile__inf profile__inf--phone-number m-0 font-weight-light fs-12"
                >
                  abc@gmail.com
                </p>
              </div>
            </div>
  
            <!--  -->
            <div class="icon d-flex align-items-center">
              <svg
                width="24"
                height="25"
                viewBox="0 0 24 25"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M12 20.5H21"
                  stroke="black"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
                <path
                  d="M16.5 4.00023C16.8978 3.6024 17.4374 3.37891 18 3.37891C18.2786 3.37891 18.5544 3.43378 18.8118 3.54038C19.0692 3.64699 19.303 3.80324 19.5 4.00023C19.697 4.19721 19.8532 4.43106 19.9598 4.68843C20.0665 4.9458 20.1213 5.22165 20.1213 5.50023C20.1213 5.7788 20.0665 6.05465 19.9598 6.31202C19.8532 6.56939 19.697 6.80324 19.5 7.00023L7 19.5002L3 20.5002L4 16.5002L16.5 4.00023Z"
                  stroke="black"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
              </svg>
            </div>
          </div>





           <!-- content 3-->
        <div class="d-flex align-items-center justify-content-between mt-3">
            <!--  -->
            <div class="d-flex align-items-center">
              <div class="icon d-flex align-items-center mr-3">
                <svg width="48" height="49" viewBox="0 0 48 49" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect y="0.5" width="48" height="48" rx="24" fill="#414042"/>
                    <path d="M33 22.5C33 29.5 24 35.5 24 35.5C24 35.5 15 29.5 15 22.5C15 20.1131 15.9482 17.8239 17.636 16.136C19.3239 14.4482 21.6131 13.5 24 13.5C26.3869 13.5 28.6761 14.4482 30.364 16.136C32.0518 17.8239 33 20.1131 33 22.5Z" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M24 25.5C25.6569 25.5 27 24.1569 27 22.5C27 20.8431 25.6569 19.5 24 19.5C22.3431 19.5 21 20.8431 21 22.5C21 24.1569 22.3431 25.5 24 25.5Z" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                    
              </div>
              <div>
                <p class="profile__title m-0 font-weight-bold fs-14">
                  Äá»a chá»
                </p>
                <p
                  class="profile__inf profile__inf--phone-number m-0 font-weight-light fs-12"
                >
                  HÃ  Ná»i
                </p>
              </div>
            </div>
  
            <!--  -->
            <div class="icon d-flex align-items-center">
              <svg
                width="24"
                height="25"
                viewBox="0 0 24 25"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M12 20.5H21"
                  stroke="black"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
                <path
                  d="M16.5 4.00023C16.8978 3.6024 17.4374 3.37891 18 3.37891C18.2786 3.37891 18.5544 3.43378 18.8118 3.54038C19.0692 3.64699 19.303 3.80324 19.5 4.00023C19.697 4.19721 19.8532 4.43106 19.9598 4.68843C20.0665 4.9458 20.1213 5.22165 20.1213 5.50023C20.1213 5.7788 20.0665 6.05465 19.9598 6.31202C19.8532 6.56939 19.697 6.80324 19.5 7.00023L7 19.5002L3 20.5002L4 16.5002L16.5 4.00023Z"
                  stroke="black"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
              </svg>
            </div>
          </div>





           <!-- content 4-->
        <div class="d-flex align-items-center justify-content-between mt-3">
            <!--  -->
            <div class="d-flex align-items-center">
              <div class="icon d-flex align-items-center mr-3">
                <svg width="48" height="49" viewBox="0 0 48 49" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect y="0.5" width="48" height="48" rx="24" fill="#414042"/>
                    <path d="M32 19.5H16C14.8954 19.5 14 20.3954 14 21.5V31.5C14 32.6046 14.8954 33.5 16 33.5H32C33.1046 33.5 34 32.6046 34 31.5V21.5C34 20.3954 33.1046 19.5 32 19.5Z" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M28 33.5V17.5C28 16.9696 27.7893 16.4609 27.4142 16.0858C27.0391 15.7107 26.5304 15.5 26 15.5H22C21.4696 15.5 20.9609 15.7107 20.5858 16.0858C20.2107 16.4609 20 16.9696 20 17.5V33.5" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                    
              </div>
              <div>
                <p class="profile__title m-0 font-weight-bold fs-14">
                  Nghá» nghiá»p
                </p>
                <p
                  class="profile__inf profile__inf--phone-number m-0 font-weight-light fs-12"
                >
                  Tá»± do
                </p>
              </div>
            </div>
  
            <!--  -->
            <div class="icon d-flex align-items-center">
              <svg
                width="24"
                height="25"
                viewBox="0 0 24 25"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M12 20.5H21"
                  stroke="black"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
                <path
                  d="M16.5 4.00023C16.8978 3.6024 17.4374 3.37891 18 3.37891C18.2786 3.37891 18.5544 3.43378 18.8118 3.54038C19.0692 3.64699 19.303 3.80324 19.5 4.00023C19.697 4.19721 19.8532 4.43106 19.9598 4.68843C20.0665 4.9458 20.1213 5.22165 20.1213 5.50023C20.1213 5.7788 20.0665 6.05465 19.9598 6.31202C19.8532 6.56939 19.697 6.80324 19.5 7.00023L7 19.5002L3 20.5002L4 16.5002L16.5 4.00023Z"
                  stroke="black"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
              </svg>
            </div>
          </div>





           <!-- content 5-->
        <div class="d-flex align-items-center justify-content-between mt-3">
            <!--  -->
            <div class="d-flex align-items-center">
              <div class="icon d-flex align-items-center mr-3">
                <svg width="48" height="49" viewBox="0 0 48 49" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect y="0.5" width="48" height="48" rx="24" fill="#414042"/>
                    <path d="M31 23.5H17C15.8954 23.5 15 24.3954 15 25.5V32.5C15 33.6046 15.8954 34.5 17 34.5H31C32.1046 34.5 33 33.6046 33 32.5V25.5C33 24.3954 32.1046 23.5 31 23.5Z" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M19 23.5V19.5C19 18.1739 19.5268 16.9021 20.4645 15.9645C21.4021 15.0268 22.6739 14.5 24 14.5C25.3261 14.5 26.5979 15.0268 27.5355 15.9645C28.4732 16.9021 29 18.1739 29 19.5V23.5" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                    
              </div>
              <div>
                <p class="profile__title m-0 font-weight-bold fs-14">
                  Máº­t kháº©u
                </p>
                <div class="d-flex align-items-center">
                    <p
                  class="profile__inf profile__inf--phone-number m-0 font-weight-light fs-12 mr-2"
                >
                  ************
                </p>

                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M1 12C1 12 5 4 12 4C19 4 23 12 23 12C23 12 19 20 12 20C5 20 1 12 1 12Z" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M12 15C13.6569 15 15 13.6569 15 12C15 10.3431 13.6569 9 12 9C10.3431 9 9 10.3431 9 12C9 13.6569 10.3431 15 12 15Z" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                    
                </div>
              </div>
            </div>
  
            <!--  -->
            <div class="icon d-flex align-items-center">
              <svg
                width="24"
                height="25"
                viewBox="0 0 24 25"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M12 20.5H21"
                  stroke="black"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
                <path
                  d="M16.5 4.00023C16.8978 3.6024 17.4374 3.37891 18 3.37891C18.2786 3.37891 18.5544 3.43378 18.8118 3.54038C19.0692 3.64699 19.303 3.80324 19.5 4.00023C19.697 4.19721 19.8532 4.43106 19.9598 4.68843C20.0665 4.9458 20.1213 5.22165 20.1213 5.50023C20.1213 5.7788 20.0665 6.05465 19.9598 6.31202C19.8532 6.56939 19.697 6.80324 19.5 7.00023L7 19.5002L3 20.5002L4 16.5002L16.5 4.00023Z"
                  stroke="black"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
              </svg>
            </div>
          </div>






           



        
      </div>
    </main>
    <footer></footer>

    <jsp:include page="/WEB-INF/views/common/js.jsp"></jsp:include>
  </body>
</html>
