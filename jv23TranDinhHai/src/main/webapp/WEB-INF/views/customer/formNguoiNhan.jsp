        <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

 <jsp:include page = "/WEB-INF/views/common/variables.jsp"></jsp:include>

<%-- <!-- import JSTL -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
 --%>

<!-- import sf: spring-form -->
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   
    <jsp:include page="/WEB-INF/views/common/css.jsp"></jsp:include>
    <title>Form thông tin người nhận</title>
</head>
<body>
    <header>

        <jsp:include page = "/WEB-INF/views/common/header.jsp"></jsp:include>

    </header>
    <main style="background: rgba(255, 251, 146, 0.07);" class="pb-100 pt-100">
        <div class="container">
            <sf:form enctype="multipart/form-data" modelAttribute="formNguoiNhanDTO" method = "post" action="${base}/form-nguoi-nhan-post" class="box-shadow-col rounded-24 p-4 ">
                <p class="font-weight-bold">Thông tin người nhận</p>
                <!--  -->
                <div>
                    <p class="m-0">Họ và tên</p>
                    <sf:input path = "fullName" type="text" class="w-100 bg-transparent outline-none border-bottom-input"></sf:input>
                </div>
                <!--  -->
                <div>
                    <p class="m-0 mt-3">Số điện thoại</p>
                    <sf:input path = "phoneNhan" type="number" class="w-100 bg-transparent outline-none border-bottom-input"></sf:input>
                </div>

                 <!--  -->
                 <div>
                    <p class="m-0 mt-3">Số CCCD/CMND người nhận</p>
                    <sf:input path = "soHoSo" type="number" class="w-100 bg-transparent outline-none border-bottom-input"></sf:input>
                </div>

                 <!--  -->
                 <div>
                    <p class="m-0 mt-3">Địa chỉ</p>
                    <sf:input path = "address" type="text" class="w-100 bg-transparent outline-none border-bottom-input"></sf:input>
                </div>

                <p class="font-weight-bold mt-4 mb-0">Mã bảo mật thông tin người gửi</p>
                <p class="font-weight-light m-0" style="font-size: 12px;">
                Mã này cung cấp cho người nhận,
                 khi nhận tiền người nhận cung cấp mã này cho nhân viên để được nhận tiền
                </p>
                  <!--  -->
                  <div>
                   
                    <sf:input path = "code" type="number" class="w-100 bg-transparent outline-none border-bottom-input"></sf:input>
                </div>
                
                <div class="mt-4 d-flex justify-content-end">
                    <button class="border-solid-primary bg-transparent rounded-24 px-4 py-2 cursor-pointer"><a href="${base }/home" class="text-decoration-none">Quay lại</a></button>
                <button type = "submit" class="bg-primary-color border-0 rounded-24 px-4 py-2 outline-none cursor-pointer ml-4">Tiếp tục</button>
                    
                </div>
             

            </sf:form>       
        </div>
            
    </main>
    <footer></footer>
    <jsp:include page="/WEB-INF/views/common/js.jsp"></jsp:include>
</body>
</html>