<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<jsp:include page="/WEB-INF/views/common/variables.jsp"></jsp:include>

 <div class = "header__container container">
            <div class = "header__logo">
            <img alt = "" src = "${base }/imgs/logo.svg" />
            </div>
            <div class = "header_menu">
            <ul>
            <li class = "menu__active"><a href = "${base }/home">Trang chủ</a></li>
            <li class="dropdown show">
            <a class="btn btn-secondary dropdown-toggle bg-transparent border-0 text-dark outline-none" 
              href="#" 
              role="button" 
              id="dropdownMenuLink" 
              data-toggle="dropdown" 
              aria-haspopup="true" 
              aria-expanded="false"
             >Giao dịch</a>
            
             <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
              <a class="dropdown-item" href="${base }/chuyen-tien-noi-bo">Chuyển tiền nội bộ</a>
               <a class="dropdown-item" href="${base }/form-nguoi-nhan">Chuyển tiền hộ</a>
               <a class="dropdown-item" href="${base }/nap-tien">Nạp tiền vào tài khoản</a>
               <a class="dropdown-item" href="#">Gửi tiết kiệm</a>
               <a class="dropdown-item" href="#">Vay vốn</a>


  </div>
            
            </li>
            <li><a href = "#">Về chúng tôi</a></li>
            </ul>
            
            </div>
            <div class = "header__select d-flex align-items-center">
           <div class = "header__user d-flex align-items-center">
              <div class = "">
              <svg width="24" height="27" viewBox="0 0 24 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <g clip-path="url(#clip0_2477_1349)">
                  <path d="M12 13.5C13.8186 13.5 15.5628 12.7888 16.8487 11.523C18.1347 10.2571 18.8571 8.54021 18.8571 6.75C18.8571 4.95979 18.1347 3.2429 16.8487 1.97703C15.5628 0.711159 13.8186 0 12 0C10.1814 0 8.43723 0.711159 7.15127 1.97703C5.8653 3.2429 5.14286 4.95979 5.14286 6.75C5.14286 8.54021 5.8653 10.2571 7.15127 11.523C8.43723 12.7888 10.1814 13.5 12 13.5ZM9.55179 16.0312C4.275 16.0312 0 20.2395 0 25.4338C0 26.2986 0.7125 27 1.59107 27H22.4089C23.2875 27 24 26.2986 24 25.4338C24 20.2395 19.725 16.0312 14.4482 16.0312H9.55179Z" fill="black"/>
                  </g>
                  <defs>
                  <clipPath id="clip0_2477_1349">
                  <rect width="24" height="27" fill="white"/>
                  </clipPath>
                  </defs>
              </svg>


              </div>

              <div class = "header__user--name mr-3 dropdown show">
                  <a class="btn btn-secondary dropdown-toggle" 
                   href="#" role="button" id="dropdownMenuLink"
                     data-toggle="dropdown" aria-haspopup="true" 
                      aria-expanded="false">
                         Hải
                   </a>

                  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                  <a class="dropdown-item" href="${base }/profile">Hồ sơ</a>
                  <a class="dropdown-item" href="${base }/lich-su">Lịch sử giao dịch</a>
                   <a class="dropdown-item" href="${sbase }/login">Đăng xuất</a>
                    </div>           
              </div>

              <div class = "header__user--so-du">10000000</div>
           
           
           </div>
            
            <select class = "header__language">
            <option>
                VND
            </option>
            <option>
            
               USA
            </option>
            </select>
            </div>
        </div>