
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!-- SPRING FORM -->
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<!-- JSTL -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>



<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <jsp:include page = "/WEB-INF/views/common/variables.jsp"></jsp:include>
    <jsp:include page="/WEB-INF/views/common/css.jsp"></jsp:include>
    <link rel="stylesheet" href="${base }/css/adminStyle.css" />
    <title>Danh sách tài khoản</title>
  </head>
  <body class="row">
    <nav style="height: 100vh;" class="col-3 h-auto bg-dark pr-0">
      <jsp:include page="/WEB-INF/views/common/navAdmin.jsp"></jsp:include>
    </nav>
    <main class="col-9 p-0">
      <header>
         <jsp:include page="/WEB-INF/views/common/headerAdmin.jsp"></jsp:include>
      </header>

<!-- content -->
      <form action = "${base}/admin/user/list" method  = "get" class="warpper p-4">
        <div class="category d-flex cursor-pointer">

        <input id = "search_page" type = "text" class="rounded bg-transperant mr-2 fs-14 pl-2" placeholder="Page" value = "${userSearch.currentPage }"  name = "currentPage" id = "currentPage"/>
          <input value = "${userSeach.keyword }" name = "keyword"  id = "keyword" class="rounded bg-transperant mr-2 fs-14 pl-2" type="text" placeholder="search">
          <select  name = "statusId" id = "statusId" class="fs-14 px-2 rounded mr-2" >
          
            <option value="${0 }">All</option>
            <c:forEach items = "${status }" var = "item">
            <option value="${item.id}">${item.statusName }</option>
            </c:forEach>
          </select>

          <button id = "btnSearch" type = "submit" class="border-0 bg-primary-color rounded fs-12 px-2" type="submit">Search</button>
          
      </div>

    <!--Tiêu đề   -->
    <div class="d-flex align-items-center bg-primary-color rounded-24 mt-4 py-2">
          <p class="w-5 text-center m-0 font-weight-bold">STT</p>
          <p class="w-10 text-center m-0 font-weight-bold">Role</p>
          <p class="w-25 text-center m-0 font-weight-bold">Số tiền điện thoại</p>
          <p class="w-25 text-center m-0 font-weight-bold">Họ và tên</p>
          <p class="w-15 text-center m-0 font-weight-bold">Trạng thái</p>
          <p class="w-20 text-center m-0 font-weight-bold">Tuỳ chọn</p>
    </div>

    <!-- content -->
    <div>
      <!--  -->
    <c:forEach items = "${users.data}" var = "user" varStatus="loop">
    		  <div href="" class="d-flex align-items-center py-2 mt-2">
          <p class="w-5 text-center m-0 font-weight-light">${loop.index + 1 }</p>
			 <p class="w-10 text-center m-0 font-weight-light">${user.roles.roleName}</p>
          <p class="w-25 text-center m-0 font-weight-light">${user.phoneNumber}</p>
          <p class="w-25 text-center m-0 font-weight-light">${user.firstN } ${user.ten }</p>
          <p class="w-15 text-center m-0 font-weight-light">${user.statusEntioitys.statusName } </p>
 		<div class="text-dark w-20  m-0 d-flex  justify-content-center align-items-center ">
 		   <a href="${base }/admin/register-post/${user.id}" class = "px-4 bg-primary-color rounded-24 text-center mr-3" >
              <p class="m-0 text-dark">Edit</p>
          </a>
          
          
          <!-- Xoá user --> 
          <div data-toggle="modal" data-target="#exampleModal"
           class = "px-4 bg-primary-color rounded-24 text-center cursor-pointer" >
              <p class="m-0">Delete</p>
          </div>
          
          <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Xoá tài khoản</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Bạn chắc chắn muốn xoá tài khoản này
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Huỷ</button>
        <a onClick = "deleteUser(${user.id})"  class="btn btn-primary">Xác nhận</a>
      </div>
    </div>
  </div>
</div>
          
 		
 		</div>
    </div>
    </c:forEach>

    </div>

    <!-- panigation -->
    <div class="row mt-4">
							<div class="col-12 d-flex justify-content-center">
								<div id="paging"></div>
							</div>
						</div> 
    <div class="d-flex justify-content-center mt-4">
        <!-- phân trang -->
						
    </div>
       
        </form>
      </div>
      
    </main>

 <jsp:include page="/WEB-INF/views/common/js.jsp"></jsp:include>

		<script type="text/javascript">
			$( document ).ready(function() {
				 $("#statusId").val(${userSeach.statusId}); 
				$("#paging").pagination({
					currentPage: ${users.currentPage}, 	//trang hiện tại
			        items: ${users.totalItems},			//tổng số sản phẩm
			        itemsOnPage: ${users.sizeOfPage}, 	//số sản phẩm trên 1 trang
			        cssStyle: 'light-theme',
			        onPageClick: function(pageNumber, event) {
			        	$('#currentPage').val(pageNumber);
			        	$('#btnSearch').trigger('click');
					},
			    });
			});
			
			
			function deleteUser(idUser){
				
				var data = {
						id: idUser
				};
				
				jQuery.ajax({
					url: '${base}'+"/admin/user/delete",
					type: "post",
					contentType: "application/json",
					data: JSON.stringify(data),
					dataType: "json",
					success: function(jsonResult){
						location.reload();
						alert("Xoá thành công")
					},
					error: function(jqXhr, textStatus, errorMessage){
						alert("error");
					}
				})
			}
		</script>
  </body>
</html>
